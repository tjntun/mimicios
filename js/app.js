jQuery(document).ready(function($) {

    // $('html').delegate('body', 'click', function(event) {
    //     $('body').removeClass('folder-expanded');
    // });

    $.ajax({
            url: 'http://staging.kudoso.com/api/v1/sessions',
            type: 'POST',
            data: {
                email: 'parent@kudoso.com',
                password: 'password',
                device_token: '58d4c8062bee01662865591d114b5c5b'
            }
        })
        .done(function(res) {
            if (res.token) {
                localStorage.setItem('mimicios.session', JSON.stringify(res));
                $.ajax({
                        url: 'http://staging.kudoso.com/api/v1/families/' + res.family.id + '/devices/3/hsl',
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('Authorization', 'Token token=' + res.token);
                        }
                    })
                    .done(function(res) {
                        // console.log(JSON.stringify(res.hsl));
                        if (res.hsl) {
                            var hsl = res.hsl;
                            if (!hsl.wallpaper) hsl.wallpaper = 'http://www.biglittlegeek.com/wp-content/uploads/2015/02/cool-iphone-6-space-background.jpg';
                            // Apply wallpaper
                            $('body').css({
                                'background-image': 'url(' + hsl.wallpaper + ')',
                            });
                            console.log(hsl);
                            App.init(hsl);
                            App.render();
                            App.registerEvent();
                        }
                    })
                    .fail(function(res) {
                        console.log("error", res);
                    });

            }
        })
        .fail(function(res) {
            console.log(res);
        })
        .always(function() {});

});
var slider = null,
    saveTmo = null;
var App = {
    hsl: {},
    pageSize: 0, // page_width * page_height
    pageCount: 0,
    appIconWidth: 60,
    appIconHeight: 80,
    dockHeight: 150,
    expandFolderSize: 256,
    init: function(hsl) {
        var _self = this;
        this.hsl = hsl;
        this.pageSize = hsl.page_width * hsl.page_height;
        this.pageCount = hsl.pages.length;
        this.appIconWidth = window.innerWidth / hsl.page_width;

        // Resize doc height on mobile
        if(window.innerWidth <= 380) {
            this.dockHeight = 100;
        }
        this.appIconHeight = ($.documentHeight() - this.dockHeight) / hsl.page_height;
    },
    render: function() {
        var _self = this;
        _self.renderDock();
        _self.renderScreens();
        _self.calcAppSize();
        _self.calcFolderIconSize();
        _self.updateAddNewPageBtnPosition();
        _self.activeNicescroll();
    },
    renderScreens: function() {
        var _self = this;
        // console.log(_self.hsl)
        for (var i in _self.hsl.pages) {
            var apps = _self.hsl.pages[i],
                pageContent = '<ul class="rsContent mimicPage linkSortable" type="pages">';

            for (var j in apps) {
                var app = apps[j],
                    item = '';

                if (app.type == 'application') {
                    item += '<li class="icon-holder" id="' + app.bundleId + '">';
                    item += '    <div class="app-icon" style="background-image: url(' + app.icon + ')"></div>';
                    item += '    <span class="name">' + app.displayName + '</span>';
                    item += '</li>';
                } else {
                    item += '<li class="icon-holder folder" id="folder.' + j + '">';
                    item += '    <ul class="app-icon">';
                    // append app under folder
                    for (var pIdx in app.pages) {
                        var folderPage = app.pages[pIdx];
                        for (var faIdx in folderPage) {
                            var folderApp = '';
                            folderApp += '<li class="icon-holder" id="' + folderPage[faIdx].bundleId + '">'
                            folderApp += '    <div class="app-icon" style="background-image: url(' + folderPage[faIdx].icon + ')"></div>'
                            folderApp += '    <span class="name">' + folderPage[faIdx].displayName + '</span>'
                            folderApp += '</li>';
                            item += folderApp;
                        }
                    }
                    item += '    </ul>';
                    item += '    <input class="name" type="text" value="' + app.displayName + '">';
                    item += '</li>';

                }

                pageContent += item;
            }

            pageContent += '</ul>';
            $('#Screens .royalSlider').append(pageContent);
        }

        // Active royal slider
        $(".royalSlider").royalSlider({
            sliderDrag: false,
            sliderTouch: false,
            keyboardNavEnabled: true,
            minSlideOffset: 20,
            arrowsNav: false,
            navigateByClick: false
        });

        slider = $(".royalSlider").data('royalSlider');
        _self.activeSortable();
    },
    toArray: function() {
        // pages
        var hsl = {};
        hsl.page_width = this.hsl.page_width;
        hsl.page_height = this.hsl.page_height;
        hsl.folder_width = this.hsl.folder_width;
        hsl.folder_height = this.hsl.folder_height;
        hsl.pages = [];
        hsl.dock = [];
        $('#Screens .mimicPage').each(function(index, elem) {
            var page = [];
            $(this).find('> li').each(function(idx, el) {
                var app = {};

                if ($(el).hasClass('folder')) {
                    // is folder
                    app.icon_type = 'folder';
                    app.pages = [];
                    app.display_name = $(el).find('input').val();

                    var folderPage = [];
                    $(el).find('li').each(function(i, underFolderApp) {
                        folderPage.push({
                            display_name: $(underFolderApp).find('.name').text(),
                            icon_type: 'application',
                            bundle_id: $(underFolderApp).attr('id'),
                        });
                    });
                    app.pages.push(folderPage);
                } else {
                    // app
                    app.icon_type = 'application';
                    app.bundle_id = $(el).attr('id');
                    app.display_name = $(el).find('.name').text();
                }
                page.push(app);
            });

            hsl.pages.push(page);
        });

        $('#Dock li').each(function(idx, el) {
            var app = {};
            app.display_name = $(el).find('.name').text();
            app.icon_type = $(el).hasClass('folder') ? 'folder' : 'application';
            app.bundle_id = $(el).attr('id');
            hsl.dock.push(app);
        });

        return hsl;
    },
    activeSortable: function() {
        console.log('activeSortable');
        var _self = this;
        // Active sortable plugin
        var triggerChangePageOffsetRight = window.innerWidth - _self.appIconWidth + _self.appIconWidth / 3;
        var tmo = null;
        var fromPage = -1;
        $("#Home ul").sortable({
            handle: '.app-icon',
            connectWith: "#Home ul",
            forcePlaceholderSize: true,
            forceHelperSize: true,
            sort: function(event, ui) {
                if (ui.offset.left >= triggerChangePageOffsetRight) {
                    clearTimeout(tmo);
                    tmo = setTimeout(function() {
                        var to = Math.floor((window.innerWidth * fromPage + ui.offset.left + _self.appIconWidth) / window.innerWidth);
                        slider.goTo(to);
                    }, 200);
                } else {
                    if (ui.offset.left <= 0) {
                        clearTimeout(tmo);
                        tmo = setTimeout(function() {
                            var to = Math.floor((window.innerWidth * fromPage + ui.offset.left) / window.innerWidth);
                            slider.goTo(to);
                        }, 200);
                    }
                }
            },
            start: function() {
                fromPage = slider.currSlideId;
                _self.calcAppSize();
            },
            receive: function(event, ui) {
                _self.calcAppSize();
                _self.calcFolderIconSize();
            },
            stop: function() {
                _self.save();
            }
        }).disableSelection();
    },
    renderDock: function() {
        var _self = this;
        var $dock = $('#Dock');
        for (var i in _self.hsl.dock) {
            var item = '',
                app = _self.hsl.dock[i];
            item += '<li class="icon-holder" id="' + app.bundleId + '">'
            item += '    <div class="app-icon" style="background-image: url(' + app.icon + ')"></div>'
            item += '    <span class="name">' + app.displayName + '</span>'
            item += '</li>';

            $dock.append(item);
        }
    },
    registerEvent: function() {
        var _self = this;
        try {
            // Next scene            
            $('#nextScene').click(function(event) {
                slider.next();
            });
            $('#prevScene').click(function(event) {
                slider.prev();
            });
            // New page
            $('.new-page-btn').click(function(event) {
                _self.addNewPage();
            });
            // New folder
            $('.new-folder-btn').click(function() {
                var $page = $('#Screens .mimicPage').eq(slider.currSlideId);
                var item = '';
                item += '<li class="icon-holder folder linkSortable">';
                item += '    <span class="app-icon"></span>';
                item += '    <ul class="app-icon-holder"></ul>';
                item += '    <input type="text" value="Untitled" class="name" />';
                item += '</li>';
                if ($page.find('.icon-holder').length < _self.pageSize) {
                    // current page can add more icon
                    console.log('current page can add more icon');
                    _self.addNewFolder($page);
                } else {
                    var allFull = true;
                    // current page is full, look for empty page or create new
                    console.log('current page is full, look for empty page or create new');
                    $('#Screens .mimicPage').each(function(index, el) {
                        if (index != slider.currSlideId && ($(this).find('.icon-holder').length < _self.pageSize)) {
                            // have space -> append new folder
                            slider.goTo(index);
                            _self.addNewFolder($(this));
                            allFull = false;
                            return;
                        }
                    });

                    if (allFull) {
                        console.log('all page is full, create new page');
                        _self.addNewPage(item);
                    }
                }

                _self.calcAppSize();
                _self.save();
            });

            // Expand folder on click
            $('body').delegate('.folder .app-icon, .folder .name', 'click', function(event) {
                event.stopPropagation();
                $(this).parent().addClass('expand');
                $('.rsDefault .rsBullets, .new-folder-btn, .new-page-btn').css('z-index', -1);
                _self.calcFolderIconSize(true);
                $('body').addClass('folder-expanded');
            });

            $('body').delegate('.folder', 'click', function(event) {
                _self.closeFolder();
            });
        } catch (e) {
            console.log(e);
        }
    },
    activeNicescroll: function() {
        $(".folder .app-icon").niceScroll();
    },
    calcFolderIconSize: function() {
        var _self = this,
            offset = 0,
            folderPadding = 5;

        var folderRealWidth = $('.icon-holder > .app-icon').not('.folder').first().outerWidth();
        offset = 0;
        var appIconWidth = (folderRealWidth - folderPadding * 2) / _self.hsl.folder_width,
            appIconHeight = folderRealWidth / _self.hsl.folder_height;

        $(".folder ul .icon-holder").width(appIconWidth).height(appIconHeight + offset);

        // Icon under expanded folder
        offset = 20;
        folderPadding = 10;
        appIconWidth = (_self.expandFolderSize - folderPadding * 2) / _self.hsl.folder_width;
        appIconHeight = _self.expandFolderSize / _self.hsl.folder_height;
        $(".folder.expand ul .icon-holder").width(appIconWidth).height(appIconHeight + offset);
    },
    calcAppSize: function() {
        var _self = this;
        // Calc icon grid size
        var dockHeight = _self.dockHeight;
        var appIconWidth = _self.appIconWidth;
        var appIconHeight = _self.appIconHeight;
        if (appIconWidth < appIconHeight) {
            var innerIconSize = appIconWidth - 30;
        } else {
            var innerIconSize = appIconHeight - 30;
        }

        $("#Screens .mimicPage > .icon-holder").width(appIconWidth);
        $('#Screens .mimicPage > .icon-holder').height(appIconHeight);
        $('#Screens .icon-holder .app-icon').height(innerIconSize).width(innerIconSize);
    },
    updateAddNewPageBtnPosition: function() {
        var _self = this;
        var left = window.innerWidth / 2 + 6 * _self.pageCount + 6;
        $('.new-page-btn').css('left', left + 'px');
    },
    save: function() {
        // return;
        clearTimeout(saveTmo);
        var _self = this;
        saveTmo = setTimeout(function() {
            var hsl = _self.toArray();
            try {
                var res = JSON.parse(localStorage.getItem('mimicios.session'));
                hsl = JSON.stringify(hsl);
                $.ajax({
                        url: 'http://staging.kudoso.com/api/v1/families/' + res.family.id + '/devices/3/hsl',
                        method: 'POST',
                        contentType: 'application/json',
                        data: hsl,
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('Authorization', 'Token token=' + res.token);
                        }
                    })
                    .done(function(res) {
                        console.log(res);
                    })
                    .fail(function(res) {
                        console.log("error", res);
                    });
            } catch (e) {
                console.log(e);
            }
        }, 500);
    },
    closeFolder: function() {
        $('body').removeClass('folder-expanded');
        $('.folder').removeClass('expand');
        $('.rsDefault .rsBullets, .new-folder-btn, .new-page-btn').css('z-index', 1);

        this.calcFolderIconSize();
        this.save();
        this.calcAppSize();
    },
    addNewPage: function(defaultItem) {
        var _self = this;
        try {
            // if (defaultItem == undefined) defaultItem = '<li class="icon-holder"><div class="app-icon"></div><span class="name"></span></li>';
            if (defaultItem == undefined) defaultItem = '';
            var canCreate = true;
            $('.rsSlide').each(function(index, el) {
                if ($(el).find('.icon-holder').length == 0) canCreate = false;
            });
            if (canCreate) {
                slider.appendSlide('<ul class="rsContent mimicPage linkSortable">' + defaultItem + '</ul>');
                _self.pageCount++;
                _self.updateAddNewPageBtnPosition();
            }
            _self.calcAppSize();
            setTimeout(function() {
                _self.activeSortable();
            }, 500);
        } catch (e) {
            console.log(e);
        }
    },
    addNewFolder: function($el) {
        var _self = this;
        var item = '';
        item += '<li class="icon-holder folder linkSortable">';
        item += '    <ul class="app-icon"></ul>';
        item += '    <input type="text" value="Untitled" class="name" />';
        item += '</li>';
        $el.prepend(item);
        _self.activeSortable();
        _self.activeNicescroll();
    }
};
